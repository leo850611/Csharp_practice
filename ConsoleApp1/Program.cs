﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine(Solve("gh12cdy695m1"));
			Console.ReadKey();
		}

		//https://www.codewars.com/kata/numbers-in-strings/train/csharp
		public static int Solve(string s)
		{
			int t = 0;
			int ans = 0;
			int flag = 0;
			foreach (int i in s)
			{
				if (i - 48 < 10)
				{
					if (flag == 1)
					{
						t = t*10+ i - 48;
					}
					else
					{
						t = t+ i - 48;
					}
					flag = 1;
				}
				else
				{
					flag = 0;
					if (t > ans)
					{
						ans = t;
					}
					t = 0;
				}
			}

			if (t > ans)
			{
				ans = t;
			}
			return ans;
		}

		public static int SumDigits(int number)
		{
			int n = 0;
			if (number < 0){
				number = number * -1;
			}

			while (number > 0){
				int t = 0;
				t = number % 10;
				number = number / 10;
				n = n + t;
			}
			return n;
		}



		//https://www.codewars.com/kata/weight-for-weight/train/csharp
		public static string orderWeight(string strng)
		{
			String[] str = strng.Split(' ');
			int[] scores = new int[str.Length] ;
			String order = "";
			int t = 0;
			String tt = "";
			for (int i = 0; i < str.Length; i++)
			{
				scores[i] = orderAdd(str[i]);
			}

			for (int i = 0; i < str.Length; i++)
			{
				for (int j = 0; j < str.Length-1; j++)
				{
					if (checkChange(scores[j], scores[j+1],str[j],str[j+1]))
					{
						t = scores[j];
						scores[j] = scores[j + 1];
						scores[j + 1] = t;
						tt = str[j];
						str[j] = str[j + 1];
						str[j + 1] = tt;
					}
				}
			}

			for (int i = 0; i < str.Length; i++)
			{
				order = order+str[i]+" ";
			}
				return order.Trim();
		}

		public static Boolean checkChange(int i1,int i2,string s1,string s2)
		{
			bool b = false;
			if (i1 > i2)
			{
				b = true;
			}else if (i1 == i2)
			{
				int s = 0;
				if (s1.Length > s2.Length)
				{
					s = s1.Length;
				}
				else
				{
					s = s2.Length;
				}
				int i = 0,n1 = 0,n2 = 0;
				while (i < s)
				{
					try{
						n1 = Int32.Parse(s1[i]+ "");
					}
					catch{
						n1 = -1;
					}

					try{
						n2 = Int32.Parse(s2[i]+ "");
					}
					catch{
						n2 = -1;
					}

					if (n1 > n2){
						b = true;
						i = s;
					}else if (n1 < n2){
						i = s;
						break;
					}
					i++;
				}
			}
			Console.WriteLine(s1+' '+s2);
			Console.WriteLine(b);
			return b;
		}


		public static int orderAdd(string s)
		{
			int o = 0;
			for (int i = 0; i < s.Length; i++)
			{
				o = o + int.Parse(s[i]+"");
			}
			return o;
		}


		public static string Order(string words)
		{
			String[] str = words.Split(' ');
			//throw new NotImplementedException();
			String outwords = "";
			for (int j = 0; j < str.Length; j++)
			{
				for (int i = 0; i < str.Length; i++){
					if (str[i].Contains((j + 1).ToString()))
					{
						outwords = outwords + str[i] + ' ';
					}
					//if (inString(str[i], (j+1).ToString() ))
					//{
					//	outwords = outwords + str[i] +' ';
					//}
				}
			}
			Console.WriteLine(outwords);
			return outwords.Trim();
		}

		public static Boolean inString(string s , string num)
		{
			bool flag = false;
			for (int i = 0; i < s.Length; i++)
			{
				if (s[i]== num[0])
				{
					flag = true;
				}
			}
			return flag;			
		}



		public static int NbYear(int p0, double percent, int aug, int p)
		{
			// your code
			int num = 0;
			double people = p0;
			percent = percent /100;
			while ((int)people < p)
			{
				people = people + people * percent + aug;
				people = (int) people;
				num++;
			}
			return num;
		}
	}

}
